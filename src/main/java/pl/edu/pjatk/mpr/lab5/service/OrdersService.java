package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrdersService {

    public List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
        return orders.stream()
                .filter(order -> order.countItems() > 5)
                .collect(Collectors.toList());
    }

    public ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
        return orders.stream()
                .map(Order::getClientDetails)
                .sorted(Comparator.comparing(ClientDetails::getAge).reversed())
                .findFirst()
                .get();
    }

    public Order findOrderWithLongestComments(List<Order> orders) {
        return orders.stream()
                .sorted(Comparator.comparing(Order::countCommentsChars).reversed())
                .findFirst()
                .get();
    }

    public String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
        return orders.stream()
                .filter(order -> order.getClientDetails().isOverAge(18))
                .map(order -> order.getClientDetails().getFullName())
                .collect(Collectors.joining(","));
    }

    public List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
        return orders.stream()
                .filter(order -> order.getComments().startsWith("A"))
                .map(Order::getItems)
                .flatMap(List::stream)
                .map(OrderItem::getName)
                .sorted(String::compareTo)
                .collect(Collectors.toList());
    }

    public void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
        orders.stream()
                .filter(order -> order.getClientDetails().getName().startsWith("S"))
                .forEach(order -> System.out.println(order.getClientDetails().getLogin().toUpperCase()));
    }

    public Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
        return orders.stream()
                .collect(Collectors.groupingBy(Order::getClientDetails));
    }

    public Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        return orders.stream()
                .map(Order::getClientDetails)
                .collect(Collectors.partitioningBy(o -> o.isOverAge(18)));
    }
}
