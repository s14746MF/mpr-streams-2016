package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceGroupOrdersByClientTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void name() throws Exception {
        // given
        ClientDetails clientDetails1 = createClientDetails(1L);
        Order orderForClient1_1 = createOrderForClient(clientDetails1);

        ClientDetails clientDetails2 = createClientDetails(2L);
        Order orderForClient2_1 = createOrderForClient(clientDetails2);
        Order orderForClient2_2 = createOrderForClient(clientDetails2);

        ClientDetails clientDetails3 = createClientDetails(3L);
        Order orderForClient3_1 = createOrderForClient(clientDetails3);
        Order orderForClient3_2 = createOrderForClient(clientDetails3);
        Order orderForClient3_3 = createOrderForClient(clientDetails3);

        List<Order> orders = Arrays.asList(
                orderForClient1_1,
                orderForClient2_1, orderForClient2_2,
                orderForClient3_1, orderForClient3_2, orderForClient3_3
        );

        // when
        Map<ClientDetails, List<Order>> groupedClientDetails = ordersService.groupOrdersByClient(orders);

        // then
        assertThat(groupedClientDetails).hasSize(3);
        assertThat(groupedClientDetails.get(clientDetails1)).hasSize(1);
        assertThat(groupedClientDetails.get(clientDetails2)).hasSize(2);
        assertThat(groupedClientDetails.get(clientDetails3)).hasSize(3);
    }

    private static ClientDetails createClientDetails(long id) {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setId(id);
        return clientDetails;
    }

    private static Order createOrderForClient(ClientDetails clientDetails) {
        Order order = new Order();
        order.setClientDetails(clientDetails);
        return order;
    }
}