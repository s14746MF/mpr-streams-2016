package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOldTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void shouldNotFindAnyClients() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 18),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 17),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 1)
        );

        // when
        String fullNames = ordersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);

        // then
        assertThat(fullNames).isEmpty();
    }

    @Test
    public void shouldFindOnlyOneClient() throws Exception {
        // givene
        List<Order> orders = Arrays.asList(
                createOrderWithNameSurnameAndAge("Jan", "Nowak2", 19),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 18),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 17),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 1)
        );

        // when
        String fullNames = ordersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);

        // then
        assertThat(fullNames).isEqualTo("Jan Nowak2");
    }

    @Test
    public void shouldFindManyClients() throws Exception {
        // givene
        List<Order> orders = Arrays.asList(
                createOrderWithNameSurnameAndAge("Jan", "Nowak4", 21),
                createOrderWithNameSurnameAndAge("Jan", "Nowak3", 20),
                createOrderWithNameSurnameAndAge("Jan", "Nowak2", 19),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 18),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 17),
                createOrderWithNameSurnameAndAge("Jan", "Nowak", 1)
        );

        // when
        String fullNames = ordersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);

        // then
        assertThat(fullNames).isEqualTo("Jan Nowak4,Jan Nowak3,Jan Nowak2");
    }

    private static Order createOrderWithNameSurnameAndAge(String name, String surname, int age) {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setName(name);
        clientDetails.setSurname(surname);
        clientDetails.setAge(age);

        Order order = new Order();
        order.setClientDetails(clientDetails);
        return order;
    }
}