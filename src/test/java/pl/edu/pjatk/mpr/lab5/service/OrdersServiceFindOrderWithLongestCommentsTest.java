package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceFindOrderWithLongestCommentsTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void shouldFindOrderWith1000CommentsChars() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsLength(1000),
                createOrderWithCommentsLength(1),
                createOrderWithCommentsLength(100),
                createOrderWithCommentsLength(600),
                createOrderWithCommentsLength(500)
        );

        // when
        Order orderWithLongestComments = ordersService.findOrderWithLongestComments(orders);

        // then
        assertThat(orderWithLongestComments.countCommentsChars()).isEqualTo(1000);
    }

    @Test
    public void shouldFindOrderWith95CommentsChars() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsLength(10),
                createOrderWithCommentsLength(1),
                createOrderWithCommentsLength(20),
                createOrderWithCommentsLength(95),
                createOrderWithCommentsLength(28)
        );

        // when
        Order orderWithLongestComments = ordersService.findOrderWithLongestComments(orders);

        // then
        assertThat(orderWithLongestComments.countCommentsChars()).isEqualTo(95);
    }

    @Test
    public void shouldFindOrderWith2CommentsChars() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsLength(1),
                createOrderWithCommentsLength(2),
                createOrderWithCommentsLength(1),
                createOrderWithCommentsLength(1),
                createOrderWithCommentsLength(1)
        );

        // when
        Order orderWithLongestComments = ordersService.findOrderWithLongestComments(orders);

        // then
        assertThat(orderWithLongestComments.countCommentsChars()).isEqualTo(2);
    }

    private static Order createOrderWithCommentsLength(int length) {
        Order order = new Order();
        order.setComments(generateComments(length));
        return order;
    }

    private static String generateComments(int length) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++) {
            stringBuilder.append("a");
        }

        return stringBuilder.toString();
    }
}