package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServicePartitionClientsByUnderAndOver18Test {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void name() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithClientAge(17),
                createOrderWithClientAge(18),
                createOrderWithClientAge(19),
                createOrderWithClientAge(20),
                createOrderWithClientAge(20)
        );

        // when
        Map<Boolean, List<ClientDetails>> partitionClients = ordersService.partitionClientsByUnderAndOver18(orders);

        // then
        assertThat(partitionClients.get(Boolean.TRUE)).hasSize(3);
        assertThat(partitionClients.get(Boolean.FALSE)).hasSize(2);
    }

    private static Order createOrderWithClientAge(int age) {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setAge(age);

        Order order = new Order();
        order.setClientDetails(clientDetails);
        return order;
    }
}