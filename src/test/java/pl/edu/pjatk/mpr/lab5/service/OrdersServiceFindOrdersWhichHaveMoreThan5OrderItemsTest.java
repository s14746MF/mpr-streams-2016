package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceFindOrdersWhichHaveMoreThan5OrderItemsTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void shouldFindTwoOrders() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithItems(2),
                createOrderWithItems(3),
                createOrderWithItems(4),
                createOrderWithItems(5),
                createOrderWithItems(6),
                createOrderWithItems(7)
        );

        // when
        List<Order> ordersWhichHaveMoreThan5OrderItems = ordersService.findOrdersWhichHaveMoreThan5OrderItems(orders);

        // then
        assertThat(ordersWhichHaveMoreThan5OrderItems).hasSize(2);
    }

    @Test
    public void shouldFindFourOrders() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithItems(2),
                createOrderWithItems(3),
                createOrderWithItems(4),
                createOrderWithItems(5),
                createOrderWithItems(6),
                createOrderWithItems(6),
                createOrderWithItems(6),
                createOrderWithItems(7)
        );

        // when
        List<Order> ordersWhichHaveMoreThan5OrderItems = ordersService.findOrdersWhichHaveMoreThan5OrderItems(orders);

        // then
        assertThat(ordersWhichHaveMoreThan5OrderItems).hasSize(4);
    }

    @Test
    public void shouldFindZeroOrders() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithItems(1),
                createOrderWithItems(2),
                createOrderWithItems(3),
                createOrderWithItems(3),
                createOrderWithItems(3),
                createOrderWithItems(4),
                createOrderWithItems(5),
                createOrderWithItems(5),
                createOrderWithItems(5)
        );

        // when
        List<Order> ordersWhichHaveMoreThan5OrderItems = ordersService.findOrdersWhichHaveMoreThan5OrderItems(orders);

        // then
        assertThat(ordersWhichHaveMoreThan5OrderItems).isEmpty();
    }

    private static Order createOrderWithItems(int itemsSize) {
        Order order = new Order();
        order.setItems(createOrderItems(itemsSize));
        return order;
    }

    private static List<OrderItem> createOrderItems(int size) {
        List<OrderItem> orderItems = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            OrderItem orderItem = new OrderItem();
            orderItems.add(orderItem);
        }

        return orderItems;
    }
}
