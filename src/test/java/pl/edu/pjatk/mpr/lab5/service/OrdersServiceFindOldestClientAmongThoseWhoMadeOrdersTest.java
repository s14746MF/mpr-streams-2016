package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceFindOldestClientAmongThoseWhoMadeOrdersTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void shouldFind10YearsOldClient() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithClientDetails(10),
                createOrderWithClientDetails(9),
                createOrderWithClientDetails(8),
                createOrderWithClientDetails(7),
                createOrderWithClientDetails(6),
                createOrderWithClientDetails(5),
                createOrderWithClientDetails(4)
        );

        // when
        ClientDetails oldestClientAmongThoseWhoMadeOrders = ordersService.findOldestClientAmongThoseWhoMadeOrders(orders);

        // then
        assertThat(oldestClientAmongThoseWhoMadeOrders.getAge()).isEqualTo(10);
    }

    @Test
    public void shouldFind80YearsOldClient() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithClientDetails(1),
                createOrderWithClientDetails(17),
                createOrderWithClientDetails(18),
                createOrderWithClientDetails(12),
                createOrderWithClientDetails(79),
                createOrderWithClientDetails(4),
                createOrderWithClientDetails(80)
        );

        // when
        ClientDetails oldestClientAmongThoseWhoMadeOrders = ordersService.findOldestClientAmongThoseWhoMadeOrders(orders);

        // then
        assertThat(oldestClientAmongThoseWhoMadeOrders.getAge()).isEqualTo(80);
    }

    @Test
    public void shouldFind11YearsOldClient() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithClientDetails(4),
                createOrderWithClientDetails(5),
                createOrderWithClientDetails(6),
                createOrderWithClientDetails(7),
                createOrderWithClientDetails(8),
                createOrderWithClientDetails(9),
                createOrderWithClientDetails(10),
                createOrderWithClientDetails(11)
        );

        // when
        ClientDetails oldestClientAmongThoseWhoMadeOrders = ordersService.findOldestClientAmongThoseWhoMadeOrders(orders);

        // then
        assertThat(oldestClientAmongThoseWhoMadeOrders.getAge()).isEqualTo(11);
    }

    private static Order createOrderWithClientDetails(int age) {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setAge(age);

        Order order = new Order();
        order.setClientDetails(clientDetails);
        return order;
    }
}