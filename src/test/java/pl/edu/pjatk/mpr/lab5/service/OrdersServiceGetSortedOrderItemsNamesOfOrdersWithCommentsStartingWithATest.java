package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrdersServiceGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithATest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void shouldNotFindAnyItems() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsAndItemsNames("abc", ""),
                createOrderWithCommentsAndItemsNames("aBc", ""),
                createOrderWithCommentsAndItemsNames("BCD", ""),
                createOrderWithCommentsAndItemsNames("BCD", "Nazwa 1")
        );

        // when
        List<String> sortedItemsNames = ordersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);

        // then
        assertThat(sortedItemsNames).isEmpty();
    }

    @Test
    public void shouldFindOnlyOneItem() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsAndItemsNames("abc", ""),
                createOrderWithCommentsAndItemsNames("aBc", ""),
                createOrderWithCommentsAndItemsNames("BCD", ""),
                createOrderWithCommentsAndItemsNames("BCD", "Nazwa 1"),
                createOrderWithCommentsAndItemsNames("Abc", "Nazwa 1")
        );

        // when
        List<String> sortedItemsNames = ordersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);

        // then
        assertThat(sortedItemsNames).hasSize(1);
        assertThat(sortedItemsNames.get(0)).isEqualTo("Nazwa 1");
    }

    @Test
    public void shouldFindManyItems() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithCommentsAndItemsNames("abc", ""),
                createOrderWithCommentsAndItemsNames("aBc", ""),
                createOrderWithCommentsAndItemsNames("BCD", ""),
                createOrderWithCommentsAndItemsNames("BCD", "Nazwa 1"),
                createOrderWithCommentsAndItemsNames("Abc", "ANazwa 1", "SNazwa 1", "BNazwa 1"),
                createOrderWithCommentsAndItemsNames("Abc", "ANazwa 2", "SNazwa 2", "BNazwa 2")
        );

        // when
        List<String> sortedItemsNames = ordersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);

        // then
        assertThat(sortedItemsNames).hasSize(6);
        assertThat(sortedItemsNames.get(0)).isEqualTo("ANazwa 1");
        assertThat(sortedItemsNames.get(1)).isEqualTo("ANazwa 2");
        assertThat(sortedItemsNames.get(2)).isEqualTo("BNazwa 1");
        assertThat(sortedItemsNames.get(3)).isEqualTo("BNazwa 2");
        assertThat(sortedItemsNames.get(4)).isEqualTo("SNazwa 1");
        assertThat(sortedItemsNames.get(5)).isEqualTo("SNazwa 2");
    }

    private static Order createOrderWithCommentsAndItemsNames(String comments, String... itemsNames) {
        Order order = new Order();
        order.setComments(comments);
        order.setItems(createOrderItemsWithNames(itemsNames));
        return order;
    }

    private static List<OrderItem> createOrderItemsWithNames(String... names) {
        List<OrderItem> orderItems = new ArrayList<>();

        for (String name : names) {
            OrderItem orderItem = new OrderItem();
            orderItem.setName(name);

            orderItems.add(orderItem);
        }

        return orderItems;
    }
}