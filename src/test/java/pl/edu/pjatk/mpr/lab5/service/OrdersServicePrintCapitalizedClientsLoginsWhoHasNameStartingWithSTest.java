package pl.edu.pjatk.mpr.lab5.service;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Arrays;
import java.util.List;

public class OrdersServicePrintCapitalizedClientsLoginsWhoHasNameStartingWithSTest {

    private final OrdersService ordersService = new OrdersService();

    @Test
    public void name() throws Exception {
        // given
        List<Order> orders = Arrays.asList(
                createOrderWithClientName("Sname", "SLogin"),
                createOrderWithClientName("Pname", "SLogin")
        );

        // when
        ordersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
    }

    private static Order createOrderWithClientName(String name, String login) {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setName(name);
        clientDetails.setLogin(login);

        Order order = new Order();
        order.setClientDetails(clientDetails);
        return order;
    }
}